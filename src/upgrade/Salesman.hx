package upgrade;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import game.SnailInfo;

class Salesman extends Entity
{
	public var sprite:Spritemap;

	public function new(x:Float, y:Float)
	{
		super(x, y);

		sprite = new Spritemap("gfx/upgrade/salesperson.png", 18, 19);
		sprite.add("idle", [0]);
		sprite.add("talk", [0, 1], 10);
		graphic = sprite;
		layer = 0;
	}

	override public function update()
	{
		super.update();
	}

	public function switchAnim(anim:String) {
		sprite.play(anim);
	}
}