package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import game.SnailInfo;

class SnailAI extends Entity
{
	public var sprite:Spritemap;
	public var fear:Int;
	public var stamina:Int;
	private var speedStat:Int;
	private var fearStat:Int;
	private var staminaStat:Int;
	private var runTimer:Int;
	private var moveTimer:Int;
	private var fearTimer:Int;
	private var staminaTimer:Int;
	private var maxStaminaTime:Int;
	private var maxMoveTime:Int;
	private var rand:Int;
	private var hit:Bool;
	private var diff:Int;
	private var randTimer:Int;

	public function new(x:Float, y:Float, _speed:Int, _fear:Int, _stamina:Int, _diff:Int)
	{
		super(x, y);

		speedStat = _speed;
		fearStat = _fear;
		staminaStat = _stamina;
		diff = _diff;
		fear = 0;
		stamina = 100;
		hit = false;

		maxMoveTime = Std.int(60 - (_speed / 2));
		moveTimer = maxMoveTime;

		runTimer = 0;
		fearTimer = 0;
		randTimer = 0;

		maxStaminaTime = Std.int(60 + _stamina);
		staminaTimer = maxStaminaTime;

		rand = Std.random(3);

		sprite = new Spritemap("gfx/snails/snail2.png", 42, 28);
		sprite.setFrame(0, rand);
		graphic = sprite;
		layer = 3;
	}

	override public function update()
	{
		if (runTimer <= 0) {
			sprite.setFrame(0, rand);
			if (moveTimer <= 0) {
				x += 1;
				moveTimer = maxMoveTime;
			} else {
				moveTimer -= 1;
			}
		} else {
			sprite.setFrame(1, rand);
			if (moveTimer <= 0) {
				x += 1;
				moveTimer = Std.int(maxMoveTime / 2);
			} else {
				moveTimer -= 1;
			}
			runTimer -= 1;
		}

		if (staminaTimer <= 0) {
			stamina -= 1;
			staminaTimer = maxStaminaTime;
		} else {
			staminaTimer -= 1;
		}

		if (fearTimer <= 0) {
			if (fear > 0) {
				fear -= 1;
				fearTimer = 30 - fearStat;
			}
		} else { fearTimer -= 1; }

		if (randTimer > 0) {
			randTimer -= 1;
		}

		if (fear < 100 - (50 - fearStat)) {
			if (randTimer == 0 && Std.random(20000) < ((100) / (10 - Math.pow(diff,2)))) {
				hit = true;
				randTimer = Std.int(10 - Math.pow(diff,2));
			}
		}

		if (hit) {
			runTimer = 120;
			fear += 50 - fearStat;
			stamina -= Math.round(5 - (staminaStat / 10));
			fearTimer = 240 - (fearStat * 4);
			//scene.add(new Smack(x + 21, y + 14));
			hit = false;
		}
		
		super.update();
	}
}