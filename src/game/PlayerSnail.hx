package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import game.SnailInfo;

class PlayerSnail extends Entity
{
	public var sprite:Spritemap;
	public var fear:Int;
	public var stamina:Int;
	private var speedStat:Int;
	private var fearStat:Int;
	private var runTimer:Int;
	private var moveTimer:Int;
	private var fearTimer:Int;
	private var staminaTimer:Int;
	private var maxStaminaTime:Int;
	private var maxMoveTime:Int;
	private var smack:Sfx;

	public function new(x:Float, y:Float)
	{
		super(x, y);

		speedStat = SnailInfo._snailSpeed;
		fearStat = SnailInfo._snailFear;
		fear = 0;
		stamina = 100;

		maxMoveTime = Std.int(60 - (SnailInfo._snailSpeed / 2));
		moveTimer = maxMoveTime;

		runTimer = 0;
		fearTimer = 0;

		maxStaminaTime = Std.int(60 + SnailInfo._snailStamina);
		staminaTimer = maxStaminaTime;

		smack = new Sfx("snd/Explosion44.wav");

		sprite = new Spritemap("gfx/snails/snail2.png", 42, 28);
		sprite.setFrame(0, SnailInfo._snailType);
		graphic = sprite;
		layer = 1;
	}

	override public function update()
	{
		if (runTimer <= 0) {
			sprite.setFrame(0, SnailInfo._snailType);
			if (moveTimer <= 0) {
				x += 1;
				moveTimer = maxMoveTime;
			} else {
				moveTimer -= 1;
			}
		} else {
			sprite.setFrame(1, SnailInfo._snailType);
			if (moveTimer <= 0) {
				x += 1;
				moveTimer = Std.int(maxMoveTime / 2);
			} else {
				moveTimer -= 1;
			}
			runTimer -= 1;
		}

		if (staminaTimer <= 0) {
			stamina -= 1;
			staminaTimer = maxStaminaTime;
		} else {
			staminaTimer -= 1;
		}

		if (fearTimer <= 0) {
			if (fear > 0) {
				fear -= 1;
				fearTimer = 30 - SnailInfo._snailFear;
			}
		} else { fearTimer -= 1; }

		if (Input.pressed(Key.Z)) {
			smack.play();
			runTimer = 120;
			fear += 50 - SnailInfo._snailFear;
			stamina -= Math.round(5 - (SnailInfo._snailStamina / 10));
			fearTimer = 240 - (SnailInfo._snailFear * 4);
			scene.add(new Smack(x + 15, y + 14));
		}
		
		super.update();
	}
}