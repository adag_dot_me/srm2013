package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import game.SnailInfo;

class Smack extends Entity
{
	public var sprite:Spritemap;
	private var timer:Int;

	public function new(x:Float, y:Float)
	{
		super(x, y);

		timer = 30;
		sprite = new Spritemap("gfx/smack.png", 10, 10);
		graphic = sprite;
		layer = 0;
	}

	override public function update()
	{
		if (timer <= 0) {
			scene.remove(this);
		} else {
			timer -= 1;
		}
		super.update();
	}
}