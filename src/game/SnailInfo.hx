package game;

class SnailInfo {
	public static var _snailName:String;
	public static var _snailPass:String;
	public static var _snailType:Int;
	public static var _snailSpeed:Int;
	public static var _snailFear:Int;
	public static var _snailStamina:Int;
	public static var _snailWins:Int;
	public static var _snailLosses:Int;
	public static var _snailMoney:Int;

	public static function sync(something:Int):Bool {
		var cnx = sys.db.Mysql.connect({ 
            host : "",
            port : 3306,
            user : "",
            pass : "",
            socket : null,
            database : ""
        });

        var r = cnx.request("SELECT * FROM users WHERE name = '" + cnx.escape(_snailName) + "'");

        for (row in r) {
        	if (_snailPass == row.pass) {
        		if (_snailType != row.type) {
        			cnx.request("UPDATE users SET type = " + _snailType + " WHERE name = '" + _snailName + "'");
                    trace("type updated");
        		}
        		if (_snailSpeed != row.speed) {
        			cnx.request("UPDATE users SET speed = " + _snailSpeed + " WHERE name = '" + _snailName + "'");
                    trace("speed updated");
        		}
        		if (_snailFear != row.fear) {
        			cnx.request("UPDATE users SET fear = " + _snailFear + " WHERE name = '" + _snailName + "'");
                    trace("fear updated");
        		}
        		if (_snailStamina != row.stamina) {
        			cnx.request("UPDATE users SET stamina = " + _snailStamina + " WHERE name = '" + _snailName + "'");
                    trace("stamina updated");
        		}
        		if (_snailWins != row.wins) {
        			cnx.request("UPDATE users SET wins = " + _snailWins + " WHERE name = '" + _snailName + "'");
                    trace("wins updated");
        		}
        		if (_snailLosses != row.losses) {
        			cnx.request("UPDATE users SET losses = " + _snailLosses + " WHERE name = '" + _snailName + "'");
                    trace("losses updated");
        		}
        		if (_snailMoney != row.money) {
        			cnx.request("UPDATE users SET money = " + _snailMoney + " WHERE name = '" + _snailName + "'");
                    trace("money updated");
        		}
        		//cnx.request("INSERT INTO users (name, pass, type, speed, fear, stamina, wins, losses, money, extra) VALUES ('" + cnx.escape(_snailName) + "', '" + cnx.escape(_snailPass) + "', " + _snailType + ", " + _snailSpeed + ", " + _snailFear + ", " + _snailStamina + ", " + _snailWins + ", " + _snailLosses + ", " + _snailMoney + ", 'hi')");
        		cnx.close();
				return true;
        	}
        }
        cnx.close();
        return false;
	}
}