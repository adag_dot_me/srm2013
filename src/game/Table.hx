package game;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import game.SnailInfo;

class Table extends Entity
{
	public var sprite:Spritemap;

	public function new(x:Float, y:Float)
	{
		super(x, y);

		sprite = new Spritemap("gfx/table.png", 120, 10);
		graphic = sprite;
		layer = 1;
	}

	override public function update()
	{
		
		super.update();
	}
}