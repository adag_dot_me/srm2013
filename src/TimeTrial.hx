import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import haxe.xml.Fast;
import menu.TextBackground;
import menu.MenuText;
import menu.SnailPreview;
import game.SnailInfo;
import game.PlayerSnail;
import game.Table;

class TimeTrial extends Scene
{
	private var textbg:Array<TextBackground>;
	private var text:Array<MenuText>;
	private var focus:Int;
	private var menuFocus:Int;
	private var player:PlayerSnail;
	private var hits:Array<Int>;
	private var timer:Int;
	private var endTimer:Int;
	private var ended:Bool;
	private var music:Sfx;

	public function new()
	{
		player = new PlayerSnail(20, 106);

		ended = false;
		endTimer = 240;
		timer = 0;

		music = new Sfx("music/Snail_RaceTheme.ogg");

		text = new Array();
		text[0] = new MenuText("Fear: " + Std.string(player.fear) + " | Stamina: " + Std.string(player.stamina) + " | Time: " + Std.string(Std.int(timer / 60)), 80, 7, true, 24, 0x183030, "font/font.ttf");

		hits = new Array();

		super();
	}

	public override function begin()
	{	
		for (i in 0...text.length) {
			add(text[i]);
		}

		add(player);
		add(new TextBackground(0, 0, "gfx/gameui.png", 160, 16));
		add(new Table(20, 134));
		music.play();
	}

	public override function update()
	{
		text[0].menuText.text = "Fear: " + Std.string(player.fear) + " | Stamina: " + Std.string(player.stamina) + " | Time: " + Std.string(Std.int(timer / 60));
		text[0].menuText.centerOrigin();

		if (!ended) {
			timer += 1;
		}

		if ((player.fear >= 100 || player.stamina <= 0) && !ended) {
			endGame(false);
		}

		if (player.x >= 98 && !ended) {
			endGame(true);
		}

		if (ended) {
			if (endTimer <= 0) {
				if (SnailInfo.sync(0)) {
					HXP.scene = new MainMenu();
				}
			} else {
				endTimer -= 1;
			}
			//trace(endTimer);
		}

		if (Input.pressed(Key.Z)) {
			hits.push(timer);
		}
		super.update();
	}

	private function endGame(win:Bool)
	{
		remove(player);
		music.stop();

		var cnx = sys.db.Mysql.connect({ 
            host : "",
            port : 3306,
            user : "",
            pass : "",
            socket : null,
            database : ""
        });

        var r = cnx.request("SELECT * FROM multi WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
        trace("test 1");

        if (win) {
        	var hitstring = "";
        	for (i in 0...hits.length) {
        		if (i < hits.length - 1) {
        			hitstring += Std.string(hits[i]) + ", ";
        		} else {
        			hitstring += Std.string(hits[i]);
        		}
        	}
        	 trace("test 2");
        	if (r.length == 0) {
        		 trace("test 3");
        		cnx.request("INSERT INTO multi (name, type, speed, fear, stamina, hits, time) VALUES ('" + cnx.escape(SnailInfo._snailName) + "', " + SnailInfo._snailType + ", " + SnailInfo._snailSpeed + ", " + SnailInfo._snailFear + ", " + SnailInfo._snailStamina + ", '" + hitstring + "', " + timer + ")");
        		 trace("test 4");
        	} else {
        		for (row in r) {
	        		if (row.name == cnx.escape(SnailInfo._snailName) && timer > row.time) {
		        		cnx.request("UPDATE multi SET type = " + SnailInfo._snailType + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET speed = " + SnailInfo._snailSpeed + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET fear = " + SnailInfo._snailFear + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET stamina = " + SnailInfo._snailStamina + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET hits = " + hitstring + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET time = " + timer + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        	}
		        }
        	}
        	add(new TextBackground(41, 64, "gfx/textfieldbg.png", 78, 16));
        	add(new MenuText("Time submitted!", 80, 71, true, 24, 0x183030, "font/font.ttf"));
        } else {
        	var lose = new Sfx("music/Snail_Lose.ogg");
        	lose.play();
        	add(new TextBackground(41, 64, "gfx/textfieldbg.png", 78, 16));
        	add(new MenuText("Failed!", 80, 71, true, 24, 0x183030, "font/font.ttf"));
        }

        cnx.close();
        ended = true;
	}
}