package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import game.SnailInfo;

class SnailPreview extends Entity
{
	public var sprite:Spritemap;

	public function new(x:Float, y:Float)
	{
		super(x, y);

		sprite = new Spritemap("gfx/snails/snail2.png", 42, 28);
		graphic = sprite;
		layer = 0;
	}

	override public function update()
	{
		sprite.setFrame(0, SnailInfo._snailType);
		super.update();
	}

	override public function removed() {
		TitleScreen.snailremoved = true;
	}
}