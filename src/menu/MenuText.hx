package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Draw;

class MenuText extends Entity 
{
	public var menuText:Text;
	private var oldScale:Int;
	private var sizeSet:Bool;
	private var c:Bool;

	public function new(_text:String, _x:Float, _y:Float, ?center:Bool, ?_size:Int, ?_color:Int, ?_font:String)
	{
		super(_x, _y);
		//trace("super");

		menuText = new Text(_text);
		if (_color != null) {
			menuText.color = _color;
		} else {
			menuText.color = 0xFFFFFF;
		}
		menuText.resizable = true;
		//oldScale = 1;
		//sizeSet = false;
		//c = center;

		if (_size != null) {
			menuText.size = _size;
		} else {
			menuText.size = 10;
		}

		if (_font != null) {
			menuText.font = _font;
		}

		if (center == true) {
			menuText.centerOrigin();
		}

		graphic = menuText;
		x = _x;
		y = _y;
		layer = 0;
	}

	override public function update()
	{
		/*if (HXP.fullscreen) {
			if (!sizeSet) {
				menuText.size *= Std.int(HXP.screen.scaleX / 2);
				oldScale = Std.int(HXP.screen.scaleX / 2);
				//menuText.scaleY = HXP.screen.scaleY;
				this.graphic = menuText;
				
				if (c) {
					menuText.centerOrigin();
				}
				sizeSet = true;
			}
		} else {
			menuText.size = Std.int(menuText.size/oldScale);
			oldScale = 1;
			this.graphic = menuText;
			sizeSet = false;

			if (c) {
				menuText.centerOrigin();
			}
		}*/
		super.update();
	}

	public function updateAlpha(_alpha:Float) 
	{
		menuText.alpha = _alpha;
		this.graphic = menuText;
	}
}