package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import menu.MenuText;
import menu.TextBackground;

class MenuSnail extends Entity
{
	public var sprite:Spritemap;
	private var moveTimer:Int;
	private var letters:Array<MenuText>;
	private var letter:Int;
	private var bg:TextBackground;
	private var bgadded:Bool;

	public function new(x:Float, y:Float)
	{
		super(x, y);

		sprite = new Spritemap("gfx/snails/snail0.png", 42, 28);
		//sprite.setFrame(0, 0);
		graphic = sprite;
		layer = 0;
		moveTimer = 0;
		bgadded = false;

		letter = 0;
		letters = new Array();
		letters[0] = new MenuText("P", 47, 120, false, 24, 0x183030, "font/font.ttf");
		letters[1] = new MenuText("R", 53, 120, false, 24, 0x183030, "font/font.ttf");
		letters[2] = new MenuText("E", 59, 120, false, 24, 0x183030, "font/font.ttf");
		letters[3] = new MenuText("S", 65, 120, false, 24, 0x183030, "font/font.ttf");
		letters[4] = new MenuText("S", 71, 120, false, 24, 0x183030, "font/font.ttf");
		letters[5] = new MenuText(" ", 77, 120, false, 24, 0x183030, "font/font.ttf");
		letters[6] = new MenuText("S", 83, 120, false, 24, 0x183030, "font/font.ttf");
		letters[7] = new MenuText("T", 89, 120, false, 24, 0x183030, "font/font.ttf");
		letters[8] = new MenuText("A", 95, 120, false, 24, 0x183030, "font/font.ttf");
		letters[9] = new MenuText("R", 101, 120, false, 24, 0x183030, "font/font.ttf");
		letters[10] = new MenuText("T", 107, 120, false, 24, 0x183030, "font/font.ttf");

		bg = new TextBackground(45, 119, "gfx/startbg.png", 1, 16);

		for (i in 0...letters.length) {
			letters[i].layer = 1;
		}
	}

	override public function update()
	{
		if (moveTimer <= 0) {
			x += 1;
			if (x > 37 && x <= 107) {
				if (!bgadded) {
					scene.add(bg);
					bgadded = true;
				}
				bg.updateGraphic("gfx/startbg.png", Std.int(x - 37), 16);
			}
			moveTimer = 15;
		} else {
			moveTimer -= 1;
		}

		if (x + 4 == letters[letter].x) {
			scene.add(letters[letter]);

			if (letter < 10) {
				letter++;
			}
		}

		if (Input.pressed(Key.SPACE)) {
			for (i in letter...letters.length) {
				scene.add(letters[i]);
				scene.remove(this);
			}

			bg.updateGraphic("gfx/startbg.png", 70, 16);
			if (!bgadded) {
				scene.add(bg);
			}
		}
		super.update();

		if (x > 160) {
			scene.remove(this);
		}
	}

	override public function removed() {
		TitleScreen.snailremoved = true;
	}
}