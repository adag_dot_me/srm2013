package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;

class TextBackground extends Entity
{
	public var sprite:Spritemap;

	public function new(x:Float, y:Float, _sprite:String, imgx:Int, imgy:Int)
	{
		super(x, y);

		sprite = new Spritemap(_sprite, imgx, imgy);
		graphic = sprite;
		layer = 3;
	}

	public function updateGraphic(_sprite:String, imgx:Int, imgy:Int) {
		var _spr = new Spritemap(_sprite, imgx, imgy);
		graphic = _spr;
	}

	public function updateSprite(x:Int, y:Int) {
		sprite.setFrame(x, y);
	}
}