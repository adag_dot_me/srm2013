package menu;

import com.haxepunk.utils.Key;

class KeyHandler {
	function handleKey(key:Int, shift:Bool):String {
		var string = "";

		switch(key) {
			case Key.A:
				if (shift) string += "A" else string += "a";
			case Key.B:
				if (shift) string += "B" else string += "b";
			case Key.C:
				if (shift) string += "C" else string += "c";
			case Key.D:
				if (shift) string += "D" else string += "d";
			case Key.E:
				if (shift) string += "E" else string += "e";
			case Key.F:
				if (shift) string += "F" else string += "f";
			case Key.G:
				if (shift) string += "G" else string += "g";
			case Key.H:
				if (shift) string += "H" else string += "h";
			case Key.I:
				if (shift) string += "I" else string += "i";
			case Key.J:
				if (shift) string += "J" else string += "j";
			case Key.K:
				if (shift) string += "K" else string += "k";
			case Key.L:
				if (shift) string += "L" else string += "l";
			case Key.M:
				if (shift) string += "M" else string += "m";
			case Key.N:
				if (shift) string += "N" else string += "n";
			case Key.O:
				if (shift) string += "O" else string += "o";
			case Key.P:
				if (shift) string += "P" else string += "p";
			case Key.Q:
				if (shift) string += "Q" else string += "q";
			case Key.R:
				if (shift) string += "R" else string += "r";
			case Key.S:
				if (shift) string += "S" else string += "s";
			case Key.T:
				if (shift) string += "T" else string += "t";
			case Key.U:
				if (shift) string += "U" else string += "u";
			case Key.V:
				if (shift) string += "V" else string += "v";
			case Key.W:
				if (shift) string += "W" else string += "w";
			case Key.X:
				if (shift) string += "X" else string += "x";
			case Key.Y:
				if (shift) string += "Y" else string += "y";
			case Key.Z:
				if (shift) string += "Z" else string += "z";
			case Key.DIGIT_0:
				string += "0";
			case Key.DIGIT_1:
				string += "1";
			case Key.DIGIT_2:
				string += "2";
			case Key.DIGIT_3:
				string += "3";
			case Key.DIGIT_4:
				string += "4";
			case Key.DIGIT_5:
				string += "5";
			case Key.DIGIT_6:
				string += "6";
			case Key.DIGIT_7:
				string += "7";
			case Key.DIGIT_8:
				string += "8";
			case Key.DIGIT_9:
				string += "9";
		}
		return string;
	}
}