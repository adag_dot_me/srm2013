import com.haxepunk.Engine;
import com.haxepunk.HXP;

class Main extends Engine
{

	override public function init()
	{
#if debug
		//HXP.console.enable();
#end
#if (mac || linux)
		HXP.screen.scale = 3;
#end
#if windows
		HXP.screen.scale = 1.73;
#end

		//HXP.fullscreen = true;
		HXP.scene = new TitleScreen();
	}

	public static function main() { new Main(); }

}