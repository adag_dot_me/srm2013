import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import haxe.xml.Fast;
import menu.MenuSnail;

class TitleScreen extends Scene
{
	private var logo:Image;
	private var gblogo:Image;
	private var adaglogo:Image;
	private var jologo:Image;
	private var snailimg:Spritemap;
	private var state:Int;
	private var stateTimer:Int;
	private var snail:MenuSnail;
	private var snailadded:Bool;
	public static var snailremoved:Bool;
	private var music:Sfx;

	public function new()
	{
		music = new Sfx("music/Snail_Menu.ogg");
		super();
	}

	public override function begin()
	{	
		logo = new Image("gfx/logo.png");
		logo.alpha = 0;
		logo.x = 41;
		logo.y = 20;
		addGraphic(logo);
		gblogo = new Image("gfx/gbjamlogo.png");
		gblogo.alpha = 0;
		gblogo.x = 27;
		gblogo.y = 28;
		addGraphic(gblogo);
		adaglogo = new Image("gfx/adaglogo.png");
		adaglogo.alpha = 0;
		adaglogo.x = 33;
		adaglogo.y = 20;
		addGraphic(adaglogo);
		jologo = new Image("gfx/jologo.png");
		jologo.alpha = 0;
		jologo.x = 29;
		jologo.y = 80;
		addGraphic(jologo);
		snailimg = new Spritemap("gfx/snails/snail0.png", 42, 28);
		snailimg.alpha = 0;
		snailimg.x = 59;
		snailimg.y = 80;
		addGraphic(snailimg);

		snailadded = false;
		snailremoved = false;
		snail = new MenuSnail(-42, 116);

		state = 0;
		stateTimer = 60;
		music.play();
	}

	public override function update()
	{
		if (state == 0) {
			if (gblogo.alpha < 1) {
				gblogo.alpha += 0.02;
			} else {
				if (stateTimer <= 0) {
					state += 1;
				} else {
					stateTimer -= 1;
				}
			}
		} else if (state == 1) {
			if (gblogo.alpha > 0) {
				gblogo.alpha -= 0.02;
			} else {
				state += 1;
			}
		} else if (state == 2) {
 			if (adaglogo.alpha < 1) {
				adaglogo.alpha += 0.02;
			} else {
				stateTimer = 60;
				state += 1;
			}
		} else if (state == 3) {
 			if (jologo.alpha < 1) {
				jologo.alpha += 0.02;
			} else {
				if (stateTimer <= 0) {
					state += 1;
				} else {
					stateTimer -= 1;
				}
			}
		} else if (state == 4) {
 			if (adaglogo.alpha > 0 || jologo.alpha > 0) {
 				if (adaglogo.alpha > 0) {
					adaglogo.alpha -= 0.02;
				}
				if (jologo.alpha > 0) {
					jologo.alpha -= 0.02;
				}
			} else {
				state += 1;
			}
		} else if (state == 5) {
 			if (logo.alpha < 1) {
				logo.alpha += 0.02;
			} else {
				state += 1;
			}
		} else if (state == 6) {
			if (!snailadded) {
				add(snail);
				snailadded = true;
			}

			if (snailremoved) {
				if (snailimg.alpha < 1) {
					snailimg.alpha += 0.02;
				}

				if (Input.pressed(Key.SPACE)) {
					music.stop();
					HXP.scene = new SignIn();
				}
			}
		}
		super.update();
	}
}