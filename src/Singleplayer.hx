import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import haxe.xml.Fast;
import menu.TextBackground;
import menu.MenuText;
import menu.SnailPreview;
import game.SnailInfo;
import game.PlayerSnail;
import game.SnailAI;
import game.Table;

class Singleplayer extends Scene
{
	private var textbg:Array<TextBackground>;
	private var text:Array<MenuText>;
	private var focus:Int;
	private var menuFocus:Int;
	private var player:PlayerSnail;
	private var reward:Int;
	private var ended:Bool;
	private var endTimer:Int;
	private var ai:SnailAI;
	private var music:Sfx;

	public function new(_arr:Array<Dynamic>, _reward:Int, _diff:Int)
	{
		player = new PlayerSnail(20, 106);
		ai = new SnailAI(20, 106, _arr[0], _arr[1], _arr[2], _diff + 1);

		reward = _reward;

		music = new Sfx("music/Snail_RaceTheme.ogg");

		ended = false;
		endTimer = 240;

		text = new Array();
		text[0] = new MenuText("Fear: " + Std.string(player.fear) + " | Stamina: " + Std.string(player.stamina), 80, 7, true, 24, 0x183030, "font/font.ttf");

		super();
	}

	public override function begin()
	{	
		for (i in 0...text.length) {
			add(text[i]);
		}

		add(new TextBackground(0, 0, "gfx/gameui.png", 160, 16));
		add(new Table(20, 134));
		trace("hi");
		add(player);
		add(ai);
		trace("bye");
		music.play();
	}

	public override function update()
	{
		text[0].menuText.text = "Fear: " + Std.string(player.fear) + " | Stamina: " + Std.string(player.stamina);
		text[0].menuText.centerOrigin();

		if ((player.fear >= 100 || player.stamina <= 0 || ai.x >= 98) && !ended) {
			endGame(false);
		}

		if (player.x >= 98 && !ended) {
			endGame(true);
		}

		if (ended) {
			if (endTimer <= 0) {
				if (SnailInfo.sync(0)) {
					HXP.scene = new MainMenu();
				}
			} else {
				endTimer -= 1;
			}
			//trace(endTimer);
		}

		super.update();
	}

	private function endGame(win:Bool)
	{
		remove(player);
		remove(ai);
		music.stop();

		var cnx = sys.db.Mysql.connect({ 
            host : "",
            port : 3306,
            user : "",
            pass : "",
            socket : null,
            database : ""
        });

        var r = cnx.request("SELECT * FROM users WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");

        if (win) {
        	SnailInfo._snailWins += 1;
        	SnailInfo._snailMoney += reward;
        	add(new TextBackground(41, 64, "gfx/textfieldbg.png", 78, 16));
        	add(new MenuText("You win $" + reward + "!", 80, 71, true, 24, 0x183030, "font/font.ttf"));
        	cnx.request("UPDATE users SET wins = " + SnailInfo._snailWins + " WHERE name = '" + SnailInfo._snailName + "'");
        	cnx.request("UPDATE users SET money = " + SnailInfo._snailMoney + " WHERE name = '" + SnailInfo._snailName + "'");
        } else {
        	SnailInfo._snailLosses += 1;
        	cnx.request("UPDATE users SET losses = " + SnailInfo._snailLosses + " WHERE name = '" + SnailInfo._snailName + "'");
        	var lose = new Sfx("music/Snail_Lose.ogg");
        	lose.play();
        	add(new TextBackground(41, 64, "gfx/textfieldbg.png", 78, 16));
        	add(new MenuText("You lose!", 80, 71, true, 24, 0x183030, "font/font.ttf"));
        }

        cnx.close();
        ended = true;
	}
}