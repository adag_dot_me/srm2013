import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import tjson.TJSON;
import menu.TextBackground;
import menu.MenuText;
import haxe.crypto.Md5;
import game.SnailInfo;
import flash.events.KeyboardEvent;

class SignIn extends Scene
{
	private var nameString:MenuText;
	private var passString:MenuText;
	private var nameTag:MenuText;
	private var passTag:MenuText;
	private var prompt:MenuText;
	private var focus:Int;
	private var textbg:Array<TextBackground>;

	public function new()
	{
		nameString = new MenuText("", 43, 30, false, 24, 0x183030, "font/font.ttf");
		passString = new MenuText("", 43, 70, false, 24, 0x183030, "font/font.ttf");
		nameTag = new MenuText("Name:", 80, 20, true, 24, 0x183030, "font/font.ttf");
		passTag = new MenuText("Pass:", 80, 60, true, 24, 0x183030, "font/font.ttf");
		prompt = new MenuText("", 2, 130, true, 24, 0x183030, "font/font.ttf");
		textbg = new Array();
		textbg[0] = new TextBackground(41, 29, "gfx/textfieldbg.png", 78, 16);
		textbg[1] = new TextBackground(41, 69, "gfx/textfieldbg.png", 78, 16);
		textbg[2] = new TextBackground(63, 99, "gfx/enterbg.png", 34, 16);
		focus = 0;
		super();
	}

	public override function begin()
	{
		trace("yay");
		if (sys.FileSystem.exists("login.sav")) {
			trace("exists");
			prompt.menuText.text = "Please wait.";
			prompt.x = 30;
			add(prompt);

			var fin = sys.io.File.read("login.sav", false);
			var fintext = fin.readLine();
			var json = TJSON.parse(fintext);

			var cnx = sys.db.Mysql.connect({ 
	            host : "",
	            port : 3306,
	            user : "",
	            pass : "",
	            socket : null,
	            database : ""
	        });

	        var r = cnx.request("SELECT * FROM users WHERE name = '" + cnx.escape(json.login.name) + "'");

	        for (row in r) {
	        	if (json.login.pass == row.pass) {
	        		SnailInfo._snailName = row.name;
					SnailInfo._snailPass = row.pass;
					SnailInfo._snailType = row.type;
					SnailInfo._snailSpeed = row.speed;
					SnailInfo._snailFear = row.fear;
					SnailInfo._snailStamina = row.stamina;
					SnailInfo._snailWins = row.wins;
					SnailInfo._snailLosses = row.losses;
					SnailInfo._snailMoney = row.money;
	        	}
	        }
	        cnx.close();
	        HXP.scene = new MainMenu();
		} else {
			add(nameString);
			add(passString);
			add(nameTag);
			add(passTag);
			prompt.menuText.text = "Enter username and pass.";
			prompt.x = 15;
			add(prompt);
			for (i in 0...textbg.length) {
				add(textbg[i]);
			}
			var entertext = new MenuText("Enter", 65, 100, false, 24, 0x183030, "font/font.ttf");
			add(entertext);
			HXP.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
	}

	public override function update()
	{
		if (Input.pressed(Key.DOWN)) {
			if (focus < 2) {
				focus += 1;
			}
		}
		if (Input.pressed(Key.UP)) {
			if (focus > 0) {
				focus -= 1;
			}
		}

		if (Input.pressed(Key.Z)) {
			if (focus == 2) {
				if (setData(nameString.menuText.text, passString.menuText.text)) {
					HXP.scene = new MainMenu();
				}
			}
		}

		for (i in 0...textbg.length) {
			if (i == focus) {
				textbg[i].updateSprite(1, 0);
			} else {
				textbg[i].updateSprite(0, 0);
			}
		}

		super.update();
	}

	private function onKeyDown(e:KeyboardEvent) {
		var string = "";
		if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90)) {
			if (Input.check(Key.SHIFT)) {
				string = handleKey(e.keyCode, true);
			} else {
				string = handleKey(e.keyCode, false);
			}
			if (focus == 0) {
				if (nameString.menuText.text.length < 12) {
					nameString.menuText.text += string;
				}
			} else if (focus == 1) {
				if (passString.menuText.text.length < 12) {
					passString.menuText.text += string;
				}
			}
		}

		if (e.keyCode == Key.BACKSPACE) {
			if (focus == 0) {
				nameString.menuText.text = nameString.menuText.text.substr(0, nameString.menuText.text.length - 1);
			} else if (focus == 1) {
				passString.menuText.text = passString.menuText.text.substr(0, passString.menuText.text.length - 1);
			}
		}
	}

	private function setData(name:String, _pass:String):Bool {
		var cnx = sys.db.Mysql.connect({ 
            host : "",
            port : 3306,
            user : "",
            pass : "",
            socket : null,
            database : ""
        });

        /*var fout = sys.io.File.write("login.sav", false);
		var str = "{ login : { 'name' : '" + name + "', 'pass' : " + Md5.encode(pass) + " } }";
		var name = "test";
		var pass = Std.string(Md5.encode("test"));
		fout.writeString(str);
		fout.close();*/
		var pass = Md5.encode(_pass);

		var r = null;
		//var json = TJSON.parse(str);
		//trace(json.login.name + " " + json.login.pass);
        r = cnx.request("SELECT * FROM users WHERE name = '" + cnx.escape(name) + "'");
        if (r.length == 0) {
        	try {
        		cnx.request("INSERT INTO users (name, pass, type, speed, fear, stamina, wins, losses, money, extra) VALUES ('" + cnx.escape(name) + "', '" + cnx.escape(pass) + "', 0, 0, 0, 0, 0, 0, 0, 'hi')");
        	} catch (z:Dynamic) {
	        	trace(z);
	        }
        }

        var req = cnx.request("SELECT * FROM users WHERE name = '" + cnx.escape(name) + "'");
        var bool = false;
        cnx.close();
        trace(req.length);
        for (row in req) {
        	trace(row.pass);
        	if (pass == row.pass) {
        		SnailInfo._snailName = row.name;
				SnailInfo._snailPass = row.pass;
				SnailInfo._snailType = row.type;
				SnailInfo._snailSpeed = row.speed;
				SnailInfo._snailFear = row.fear;
				SnailInfo._snailStamina = row.stamina;
				SnailInfo._snailWins = row.wins;
				SnailInfo._snailLosses = row.losses;
				SnailInfo._snailMoney = row.money;
				trace("set up variables");
				bool = true;

				trace(SnailInfo._snailName);
        	}
        }

        if (bool) {
        	trace("in bool conditional");
        	var fout = sys.io.File.write("login.sav", false);
        	trace("opened for writing");
        	var str = "{ login : { 'name' : '" + name + "', 'pass' : " + pass + " } }";
			trace("str created");
			fout.writeString(str);
			trace("wrote string");
			fout.close();
			return true;
        }

        return false;
	}

	private function handleKey(key:Int, shift:Bool):String {
		var string = "";

		switch(key) {
			case Key.A:
				if (shift) string += "A" else string += "a";
			case Key.B:
				if (shift) string += "B" else string += "b";
			case Key.C:
				if (shift) string += "C" else string += "c";
			case Key.D:
				if (shift) string += "D" else string += "d";
			case Key.E:
				if (shift) string += "E" else string += "e";
			case Key.F:
				if (shift) string += "F" else string += "f";
			case Key.G:
				if (shift) string += "G" else string += "g";
			case Key.H:
				if (shift) string += "H" else string += "h";
			case Key.I:
				if (shift) string += "I" else string += "i";
			case Key.J:
				if (shift) string += "J" else string += "j";
			case Key.K:
				if (shift) string += "K" else string += "k";
			case Key.L:
				if (shift) string += "L" else string += "l";
			case Key.M:
				if (shift) string += "M" else string += "m";
			case Key.N:
				if (shift) string += "N" else string += "n";
			case Key.O:
				if (shift) string += "O" else string += "o";
			case Key.P:
				if (shift) string += "P" else string += "p";
			case Key.Q:
				if (shift) string += "Q" else string += "q";
			case Key.R:
				if (shift) string += "R" else string += "r";
			case Key.S:
				if (shift) string += "S" else string += "s";
			case Key.T:
				if (shift) string += "T" else string += "t";
			case Key.U:
				if (shift) string += "U" else string += "u";
			case Key.V:
				if (shift) string += "V" else string += "v";
			case Key.W:
				if (shift) string += "W" else string += "w";
			case Key.X:
				if (shift) string += "X" else string += "x";
			case Key.Y:
				if (shift) string += "Y" else string += "y";
			case Key.Z:
				if (shift) string += "Z" else string += "z";
			case Key.DIGIT_0:
				string += "0";
			case Key.DIGIT_1:
				string += "1";
			case Key.DIGIT_2:
				string += "2";
			case Key.DIGIT_3:
				string += "3";
			case Key.DIGIT_4:
				string += "4";
			case Key.DIGIT_5:
				string += "5";
			case Key.DIGIT_6:
				string += "6";
			case Key.DIGIT_7:
				string += "7";
			case Key.DIGIT_8:
				string += "8";
			case Key.DIGIT_9:
				string += "9";
		}
		return string;
	}
}