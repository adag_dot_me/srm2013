import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import haxe.xml.Fast;
import menu.TextBackground;
import menu.MenuText;
import menu.SnailPreview;
import game.SnailInfo;
import game.MultiAI;
import game.PlayerSnail;
import game.Table;

class Multiplayer extends Scene
{
	private var textbg:Array<TextBackground>;
	private var text:Array<MenuText>;
	private var focus:Int;
	private var menuFocus:Int;
	private var player:PlayerSnail;
	private var ai:MultiAI;
	private var hits:Array<Int>;
	private var timer:Int;
	private var endTimer:Int;
	private var ended:Bool;
	private var reward:Int;
	private var music:Sfx;

	public function new(_type:Int, _speed:Int, _fear:Int, _stamina:Int, _hits:String, _reward:Int)
	{
		var arr = _hits.split(",");
		var intArr:Array<Int> = new Array();

		for (i in 0...arr.length) {
			intArr[i] = Std.parseInt(arr[i]);
		}
		trace(intArr[0]);
		player = new PlayerSnail(20, 106);
		ai = new MultiAI(20, 106, _type, _speed, _fear, _stamina, intArr);
		music = new Sfx("music/Snail_RaceTheme.ogg");

		ended = false;
		endTimer = 240;
		timer = 0;
		reward = _reward;

		text = new Array();
		text[0] = new MenuText("Fear: " + Std.string(player.fear) + " | Stamina: " + Std.string(player.stamina), 80, 7, true, 24, 0x183030, "font/font.ttf");

		hits = new Array();

		super();
	}

	public override function begin()
	{	
		for (i in 0...text.length) {
			add(text[i]);
		}

		add(player);
		add(ai);
		add(new TextBackground(0, 0, "gfx/gameui.png", 160, 16));
		add(new Table(20, 134));
		music.play();
	}

	public override function update()
	{
		text[0].menuText.text = "Fear: " + Std.string(player.fear) + " | Stamina: " + Std.string(player.stamina);
		text[0].menuText.centerOrigin();

		if (!ended) {
			timer += 1;
		}

		if ((player.fear >= 100 || player.stamina <= 0 || ai.x >= 98) && !ended) {
			endGame(false);
		}

		if ((player.x >= 98 || ai.fear >= 100 || ai.stamina <= 0) && !ended) {
			endGame(true);
		}

		if (ended) {
			if (endTimer <= 0) {
				if (SnailInfo.sync(0)) {
					HXP.scene = new MainMenu();
				}
			} else {
				endTimer -= 1;
			}
			//trace(endTimer);
		}

		if (Input.pressed(Key.Z)) {
			hits.push(timer);
		}
		super.update();
	}

	private function endGame(win:Bool)
	{
		remove(player);
		remove(ai);
		music.stop();

		var cnx = sys.db.Mysql.connect({ 
            host : "",
            port : 3306,
            user : "",
            pass : "",
            socket : null,
            database : ""
        });

        var r = cnx.request("SELECT * FROM multi WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
        trace("test 1");

        if (win) {
        	var hitstring = "";
        	for (i in 0...hits.length) {
        		if (i < hits.length - 1) {
        			hitstring += Std.string(hits[i]) + ",";
        		} else {
        			hitstring += Std.string(hits[i]);
        		}
        	}
        	 trace("test 2");
        	if (r.length == 0) {
        		 trace("test 3");
        		cnx.request("INSERT INTO multi (name, type, speed, fear, stamina, hits, time) VALUES ('" + cnx.escape(SnailInfo._snailName) + "', " + SnailInfo._snailType + ", " + SnailInfo._snailSpeed + ", " + SnailInfo._snailFear + ", " + SnailInfo._snailStamina + ", '" + hitstring + "', " + timer + ")");
        		 trace("test 4");
        	} else {
        		for (row in r) {
	        		if (row.name == cnx.escape(SnailInfo._snailName) && timer > row.time) {
		        		cnx.request("UPDATE multi SET type = " + SnailInfo._snailType + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET speed = " + SnailInfo._snailSpeed + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET fear = " + SnailInfo._snailFear + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET stamina = " + SnailInfo._snailStamina + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET hits = " + hitstring + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        		cnx.request("UPDATE multi SET time = " + timer + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
		        	}
		        }
	        	SnailInfo._snailWins += 1;
        		SnailInfo._snailMoney += reward;

        		cnx.request("UPDATE users SET wins = " + SnailInfo._snailWins + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
        		cnx.request("UPDATE users SET money = " + SnailInfo._snailMoney + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
        	}
        	add(new TextBackground(41, 64, "gfx/textfieldbg.png", 78, 16));
        	add(new MenuText("You win $" + reward + "!", 80, 71, true, 24, 0x183030, "font/font.ttf"));
        } else {
        	SnailInfo._snailLosses += 1;
        	cnx.request("UPDATE users SET losses = " + SnailInfo._snailLosses + " WHERE name = '" + cnx.escape(SnailInfo._snailName) + "'");
        	var lose = new Sfx("music/Snail_Lose.ogg");
        	lose.play();
        	add(new TextBackground(41, 64, "gfx/textfieldbg.png", 78, 16));
        	add(new MenuText("You lose!", 80, 71, true, 24, 0x183030, "font/font.ttf"));
        }

        cnx.close();
        ended = true;
	}
}