import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import haxe.xml.Fast;
import menu.TextBackground;
import menu.MenuText;
import menu.SnailPreview;
import game.SnailInfo;
import upgrade.Salesman;

class UpgradeMenu extends Scene
{
	private var textbg:Array<TextBackground>;
	private var text:Array<MenuText>;
	private var focus:Int;
	private var menuFocus:Int;
	private var talkTimer:Int;
	private var sales:Salesman;
	private var statText:Array<MenuText>;
	private var prices:Array<Int>;
	private var priceText:Array<MenuText>;
	private var timer:Int;
	private var selectSound:Sfx;
	private var buySound:Sfx;

	public function new()
	{
		textbg = new Array();
		textbg[0] = new TextBackground(0, 0, "gfx/upgrade/speechbubble.png", 160, 22);
		textbg[1] = new TextBackground(17, 52, "gfx/menutextbg.png", 46, 16);
		textbg[2] = new TextBackground(17, 77, "gfx/menutextbg.png", 46, 16);
		textbg[3] = new TextBackground(17, 102, "gfx/menutextbg.png", 46, 16);
		textbg[4] = new TextBackground(17, 127, "gfx/menutextbg.png", 46, 16);

		text = new Array();
		text[0] = new MenuText("What can I do for you?", 80, 7, true, 24, 0x183030, "font/font.ttf");
		text[1] = new MenuText("Speed", 40, 59, true, 24, 0x183030, "font/font.ttf");
		text[2] = new MenuText("Fear", 40, 84, true, 24, 0x183030, "font/font.ttf");
		text[3] = new MenuText("Stamina", 40, 109, true, 24, 0x183030, "font/font.ttf");
		text[4] = new MenuText("Go back", 40, 134, true, 24, 0x183030, "font/font.ttf");
		text[5] = new MenuText(SnailInfo._snailName, -80, 65, true, 24, 0x183030, "font/font.ttf");

		statText = new Array();
		statText[0] = new MenuText(Std.string(SnailInfo._snailSpeed), 80, 59, true, 24, 0x183030, "font/font.ttf");
		statText[1] = new MenuText(Std.string(SnailInfo._snailFear), 80, 84, true, 24, 0x183030, "font/font.ttf");
		statText[2] = new MenuText(Std.string(SnailInfo._snailStamina), 80, 109, true, 24, 0x183030, "font/font.ttf");
		statText[3] = new MenuText("Money: $" + Std.string(SnailInfo._snailMoney), 110, 134, true, 24, 0x183030, "font/font.ttf");

		prices = new Array();
		prices[0] = (SnailInfo._snailSpeed + 1) * 100;
		prices[1] = (SnailInfo._snailFear + 1) * 100;
		prices[2] = (SnailInfo._snailStamina + 1) * 100;

		priceText = new Array();
		priceText[0] = new MenuText("$" + prices[0], 120, 59, true, 24, 0x183030, "font/font.ttf");
		priceText[1] = new MenuText("$" + prices[1], 120, 84, true, 24, 0x183030, "font/font.ttf");
		priceText[2] = new MenuText("$" + prices[2], 120, 109, true, 24, 0x183030, "font/font.ttf");

		if (SnailInfo._snailSpeed == 30) {
			priceText[0].menuText.text = "Maxed.";
			priceText[0].menuText.centerOrigin();
		}
		if (SnailInfo._snailFear == 30) {
			priceText[1].menuText.text = "Maxed.";
			priceText[1].menuText.centerOrigin();
		}
		if (SnailInfo._snailStamina == 30) {
			priceText[2].menuText.text = "Maxed.";
			priceText[2].menuText.centerOrigin();
		}

		selectSound = new Sfx("snd/Hit_Hurt72.wav");
		buySound = new Sfx("snd/Pickup_Coin18.wav");

		sales = new Salesman(141, 23);
		talkTimer = 30;
		focus = 0;
		timer = 0;

		super();
	}

	public override function begin()
	{	
		for (i in 0...textbg.length) {
			add(textbg[i]);
		}
		for (i in 0...text.length) {
			add(text[i]);
		}
		for (i in 0...statText.length) {
			add(statText[i]);
		}
		for (i in 0...priceText.length) {
			add(priceText[i]);
		}
		//SnailInfo._snailMoney += 100;
		add(sales);
	}

	public override function update()
	{
		statText[0].menuText.text = Std.string(SnailInfo._snailSpeed);
		statText[1].menuText.text = Std.string(SnailInfo._snailFear);
		statText[2].menuText.text = Std.string(SnailInfo._snailStamina);
		statText[3].menuText.text = "Money: $" + Std.string(SnailInfo._snailMoney);

		if (text[0].menuText.text != "What can I do for you?") {
			if (timer <= 0) {
				text[0].menuText.text = "Anything else?";
				text[0].menuText.centerOrigin();
			} else {
				timer -= 1;
			}
		}

		if (talkTimer <= 0) {
			sales.switchAnim("idle");
		} else {
			sales.switchAnim("talk");
			talkTimer -= 1;
		}

		if (Input.pressed(Key.DOWN)) {
			if (focus < 3) {
				focus += 1;
			}
			selectSound.play();
		}
		if (Input.pressed(Key.UP)) {
			if (focus > 0) {
				focus -= 1;
			}
			selectSound.play();
		}

		if (Input.pressed(Key.Z)) {
			if (focus == 0) {
				if (SnailInfo._snailMoney > prices[focus] && SnailInfo._snailSpeed < 30) {
					SnailInfo._snailSpeed += 1;
					SnailInfo._snailMoney -= prices[focus];
					statText[3].menuText.text = "Money: $" + Std.string(SnailInfo._snailMoney);
					statText[3].menuText.centerOrigin();
					trace("added");
					if (SnailInfo.sync(0)) {
						text[0].menuText.text = "Speed upgraded.";
						text[0].menuText.centerOrigin();
						prices[focus] = (SnailInfo._snailSpeed + 1) * 100;
						if (SnailInfo._snailSpeed < 30) {
							priceText[focus].menuText.text = "$" + Std.string(prices[focus]);
						} else {
							priceText[focus].menuText.text = "Maxed.";
						}
						priceText[focus].menuText.centerOrigin();
						talkTimer = 150;
					} else {
						SnailInfo._snailSpeed -= 1;
						SnailInfo._snailMoney += prices[focus];
						trace("failed");
					}
					timer = 120;
					buySound.play();
					trace(SnailInfo._snailSpeed);
				}
			}
			if (focus == 1) {
				if (SnailInfo._snailMoney > prices[focus] && SnailInfo._snailFear < 30) {
					SnailInfo._snailFear += 1;
					SnailInfo._snailMoney -= prices[focus];
					statText[3].menuText.text = "Money: $" + Std.string(SnailInfo._snailMoney);
					statText[3].menuText.centerOrigin();
					trace("added");
					if (SnailInfo.sync(0)) {
						text[0].menuText.text = "Fear reduction upgraded.";
						text[0].menuText.centerOrigin();
						prices[focus] = (SnailInfo._snailFear + 1) * 100;
						if (SnailInfo._snailFear < 30) {
							priceText[focus].menuText.text = "$" + Std.string(prices[focus]);
						} else {
							priceText[focus].menuText.text = "Maxed.";
						}
						priceText[focus].menuText.centerOrigin();
						talkTimer = 150;
					} else {
						SnailInfo._snailFear -= 1;
						trace("failed");
					}
					timer = 120;
					buySound.play();
					trace(SnailInfo._snailFear);
				}
			}
			if (focus == 2) {
				if (SnailInfo._snailMoney > prices[focus] && SnailInfo._snailStamina < 30) {
					SnailInfo._snailStamina += 1;
					SnailInfo._snailMoney -= prices[focus];
					statText[3].menuText.text = "Money: $" + Std.string(SnailInfo._snailMoney);
					statText[3].menuText.centerOrigin();
					trace("added");
					if (SnailInfo.sync(0)) {
						text[0].menuText.text = "Stamina upgraded.";
						text[0].menuText.centerOrigin();
						prices[focus] = (SnailInfo._snailStamina + 1) * 100;
						if (SnailInfo._snailStamina < 30) {
							priceText[focus].menuText.text = "$" + Std.string(prices[focus]);
						} else {
							priceText[focus].menuText.text = "Maxed.";
						}
						priceText[focus].menuText.centerOrigin();
						talkTimer = 150;
					} else {
						SnailInfo._snailStamina -= 1;
						trace("failed");
					}
					timer = 120;
					buySound.play();
					trace(SnailInfo._snailStamina);
				}
			}
			if (focus == 3) {
				if (SnailInfo.sync(0)) {
					HXP.scene = new MainMenu();
				}
			}
		}

		for (i in 0...textbg.length) {
			if (i == focus + 1) {
				textbg[i].updateSprite(1, 0);
			} else {
				textbg[i].updateSprite(0, 0);
			}
		}

		super.update();
	}
}