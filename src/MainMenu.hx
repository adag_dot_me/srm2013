import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;
import haxe.xml.Fast;
import menu.TextBackground;
import menu.MenuText;
import menu.SnailPreview;
import game.SnailInfo;

class MainMenu extends Scene
{
	private var textbg:Array<TextBackground>;
	private var text:Array<MenuText>;
	private var focus:Int;
	private var menuFocus:Int;
	private var randomOpponents:Array<Array<Int>>;
	private var randomMoney:Array<Int>;
	private var multiOpponents:Array<Array<Dynamic>>;
	private var multiMoney:Array<Int>;
	private var selectSound:Sfx;
	private var focusSound:Sfx;

	public function new()
	{
		focus = 0;
		menuFocus = 0;

		randomOpponents = new Array();
		randomMoney = new Array();

		selectSound = new Sfx("snd/Hit_Hurt72.wav");
		focusSound = new Sfx("snd/Powerup30.wav");

		for (i in 0...3) {
			var max = (10 * (i + 1)) + 1;
			var min = (10 * (i)) + 1;
			randomOpponents[i] = new Array();
			randomOpponents[i][0] = Std.int(randomRange(min, max));
			randomOpponents[i][1] = Std.int(randomRange(min, max));
			randomOpponents[i][2] = Std.int(randomRange(min, max));
			randomMoney[i] = 3 * (randomOpponents[i][0] + randomOpponents[i][1] + randomOpponents[i][2]);
		}

		multiOpponents = new Array();
		getTopPlayers(multiOpponents);

		textbg = new Array();
		textbg[0] = new TextBackground(57, 39, "gfx/menutextbg.png", 46, 16);
		textbg[1] = new TextBackground(57, 64, "gfx/menutextbg.png", 46, 16);
		textbg[2] = new TextBackground(57, 89, "gfx/menutextbg.png", 46, 16);
		textbg[3] = new TextBackground(217, 52, "gfx/menutextbg.png", 46, 16);
		textbg[4] = new TextBackground(217, 76, "gfx/menutextbg.png", 46, 16);
		textbg[5] = new TextBackground(361, 39, "gfx/textfieldbg.png", 78, 16);
		textbg[6] = new TextBackground(361, 64, "gfx/textfieldbg.png", 78, 16);
		textbg[7] = new TextBackground(361, 89, "gfx/textfieldbg.png", 78, 16);
		textbg[8] = new TextBackground(521, 27, "gfx/textfieldbg.png", 78, 16);
		textbg[9] = new TextBackground(521, 52, "gfx/textfieldbg.png", 78, 16);
		textbg[10] = new TextBackground(521, 77, "gfx/textfieldbg.png", 78, 16);
		textbg[11] = new TextBackground(521, 102, "gfx/textfieldbg.png", 78, 16);

		text = new Array();
		text[0] = new MenuText("Play", 80, 46, true, 24, 0x183030, "font/font.ttf");
		text[1] = new MenuText("Upgrade", 80, 71, true, 24, 0x183030, "font/font.ttf");
		text[2] = new MenuText("Snail", 80, 96, true, 24, 0x183030, "font/font.ttf");
		text[3] = new MenuText("Single", 240, 59, true, 24, 0x183030, "font/font.ttf");
		text[4] = new MenuText("Multi", 240, 83, true, 24, 0x183030, "font/font.ttf");
		text[5] = new MenuText(SnailInfo._snailName, -80, 65, true, 24, 0x183030, "font/font.ttf");
		text[6] = new MenuText("Wins - " + SnailInfo._snailWins, -80, 75, true, 24, 0x183030, "font/font.ttf");
		text[7] = new MenuText("Losses - " + SnailInfo._snailLosses, -80, 85, true, 24, 0x183030, "font/font.ttf");
		text[8] = new MenuText("Win ratio - " + (SnailInfo._snailWins / SnailInfo._snailLosses), -80, 95, true, 24, 0x183030, "font/font.ttf");
		text[9] = new MenuText("<", -107, 35, false, 24, 0x183030, "font/font.ttf");
		text[10] = new MenuText(">", -59, 35, false, 24, 0x183030, "font/font.ttf");
		text[11] = new MenuText("Easy - $" + Std.string(randomMoney[0]), 400, 46, true, 24, 0x183030, "font/font.ttf");
		text[12] = new MenuText("Medium - $" + Std.string(randomMoney[1]), 400, 71, true, 24, 0x183030, "font/font.ttf");
		text[13] = new MenuText("Hard - $" + Std.string(randomMoney[2]), 400, 96, true, 24, 0x183030, "font/font.ttf");
		text[14] = new MenuText("Time Trial", 560, 34, true, 24, 0x183030, "font/font.ttf");
		if (multiOpponents[0] != null) {
			text[15] = new MenuText(multiOpponents[0][0] + " - $" + Std.string(3 * (multiOpponents[0][2] + multiOpponents[0][3] + multiOpponents[0][4])), 560, 59, true, 24, 0x183030, "font/font.ttf");
		} else { text[15] = new MenuText("Not available", 560, 59, true, 24, 0x183030, "font/font.ttf"); }
		if (multiOpponents[1] != null) {
			text[16] = new MenuText(multiOpponents[1][0] + " - $" + Std.string(3 * (multiOpponents[1][2] + multiOpponents[1][3] + multiOpponents[1][4])), 560, 84, true, 24, 0x183030, "font/font.ttf");
		} else { text[16] = new MenuText("Not available", 560, 84, true, 24, 0x183030, "font/font.ttf"); }
		if (multiOpponents[2] != null) {
			text[17] = new MenuText(multiOpponents[2][0] + " - $" + Std.string(3 * (multiOpponents[2][2] + multiOpponents[2][3] + multiOpponents[2][4])), 560, 109, true, 24, 0x183030, "font/font.ttf");
		} else { text[17] = new MenuText("Not available", 560, 109, true, 24, 0x183030, "font/font.ttf"); }

		super();
	}

	public override function begin()
	{	
		for (i in 0...textbg.length) {
			add(textbg[i]);
		}
		for (i in 0...text.length) {
			add(text[i]);
		}
		var pre = new SnailPreview(-101, 30);
		add(pre);
	}

	public override function update()
	{
		text[6].menuText.text = "Wins - " + SnailInfo._snailWins;
		if (SnailInfo._snailType == 0) {
			text[9].menuText.text = "";
		} else {
			text[9].menuText.text = "<";
		}

		if (SnailInfo._snailType == 2) {
			text[10].menuText.text = "";
		} else {
			text[10].menuText.text = ">";
		}

		if (HXP.camera.x > menuFocus * 160) {
			HXP.camera.x -= 10;
		} else if (HXP.camera.x < menuFocus * 160) {
			HXP.camera.x += 10;
		}

		if (Input.pressed(Key.DOWN)) {
			if (menuFocus == 0 || menuFocus == 2) {
				if (focus < 2) {
					focus += 1;
				}
			}

			if (menuFocus == 1) {
				if (focus < 1) {
					focus += 1;
				}
			}

			if (menuFocus == 3) {
				if (focus < 3) {
					focus += 1;
				}
			}

			if (menuFocus == -1) {

			}
			selectSound.play();
		}
		if (Input.pressed(Key.UP)) {
			if (focus > 0) {
				focus -= 1;
			}
			selectSound.play();
		}

		if (menuFocus < 0) {
			if (Input.pressed(Key.LEFT)) {
				if (SnailInfo._snailType > 0) {
					SnailInfo._snailType -= 1;
				}
				selectSound.play();
			}
			if (Input.pressed(Key.RIGHT)) {
				if (SnailInfo._snailType < 2) {
					SnailInfo._snailType += 1;
				}
				selectSound.play();
			}
		}

		for (i in 0...textbg.length) {
			if (menuFocus == 0) {
				if (i == focus) {
					textbg[i].updateSprite(1, 0);
				} else {
					textbg[i].updateSprite(0, 0);
				}
			}
			if (menuFocus == 1) {
				if (i == focus + 3) {
					textbg[i].updateSprite(1, 0);
				} else {
					textbg[i].updateSprite(0, 0);
				}
			}
			if (menuFocus == 2) {
				if (i == focus + 5) {
					textbg[i].updateSprite(1, 0);
				} else {
					textbg[i].updateSprite(0, 0);
				}
			}
			if (menuFocus == 3) {
				if (i == focus + 8) {
					textbg[i].updateSprite(1, 0);
				} else {
					textbg[i].updateSprite(0, 0);
				}
			}
		}

		if (Input.pressed(Key.Z)) {
			if (menuFocus == 0) {
				if (focus == 0) {
					focusSound.play();
					menuFocus += 1;
				} else if (focus == 1) {
					if (SnailInfo.sync(0)) {
						HXP.scene = new UpgradeMenu();
					}
				} else if (focus == 2) {
					focusSound.play();
					menuFocus -= 1;
				}
			} else if (menuFocus == 1) {
				if (focus == 0) {
					//if (SnailInfo.sync(0)) {
						focusSound.play();
						menuFocus += 1;
					//}
				} else if (focus == 1) {
					//if (SnailInfo.sync(0)) {
						focusSound.play();
						menuFocus += 2;
					//}
				}
			} else if (menuFocus == -1) {
				if (SnailInfo.sync(0)) {
					focusSound.play();
					menuFocus += 1;
				}
			} else if (menuFocus == 2) {
				if (SnailInfo.sync(0)) {
					HXP.scene = new Singleplayer(randomOpponents[focus], randomMoney[focus], focus);
				}
			} else if (menuFocus == 3) {
				if (focus == 0) {
					if (SnailInfo.sync(0)) {
						HXP.scene = new TimeTrial();
					}
				} else {
					if (SnailInfo.sync(0)) {
						HXP.scene = new Multiplayer(multiOpponents[focus-1][1], multiOpponents[focus-1][2], multiOpponents[focus-1][3], multiOpponents[focus-1][4], multiOpponents[focus-1][5], Std.int(3 * (multiOpponents[focus-1][2] + multiOpponents[focus-1][3] + multiOpponents[focus-1][4])));
					}
				}
			}
			focus = 0;
		}

		if (Input.pressed(Key.X)) {
			if (menuFocus > 0) {
				focusSound.play();
				if (menuFocus == 3) {
					menuFocus -= 1;
				}
				menuFocus -= 1;
			}
			if (menuFocus < 0) {
				if (SnailInfo.sync(0)) {
					focusSound.play();
					menuFocus += 1;
				}
			}
			focus = 0;
		}

		super.update();
	}

	private function randomRange(min:Int, max:Int) {
		return min + (Math.random() * ((max - min) + 1));
  	}

  	private function getTopPlayers(arr:Array<Array<Dynamic>>) {
        var cnx = sys.db.Mysql.connect({ 
            host : "",
            port : 3306,
            user : "",
            pass : "",
            socket : null,
            database : ""
        });
        trace("1");

        var r = cnx.request("SELECT * FROM multi ORDER BY time DESC");
        trace("2");

        var a = 0;

        for (row in r) {
            if (a < 3) {
                arr[a] = new Array();
                arr[a][0] = row.name;
                arr[a][1] = row.type;
                arr[a][2] = row.speed;
                arr[a][3] = row.fear;
                arr[a][4] = row.stamina;
                arr[a][5] = row.hits;

                trace(arr[a][0]);

                a++;
            } else {
                break;
            }
        }
    }
}